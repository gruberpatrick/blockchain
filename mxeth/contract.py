
from mxeth.encryption import Encryption

class Contract:

  _address = None
  _abi = None
  _contract = None
  _key = None
  _eth_lib = None

  def __init__(self, address, abi, contract, eth_lib, key=None):
    self._address = address
    self._abi = abi
    self._contract = contract
    self._eth_lib = eth_lib
    if key == None: self.generateKey()
    else: self._key = key

  def generateKey(self):
    k = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(200))
    self._key = hashlib.sha256(k.encode()).digest()

  def call(self, function, decrypt, *params):
    try:
      result = self._contract.call().__getattr__(function)(*params)
      if decrypt: return Encryption.decryptAES(result, self._key)
      return result
    except:
      print("[ETH][CONTRACT][ERROR] Could not execute function.")
    return False

  def transact(self, account, function, *params):
    try:
      if(account.getBalance() <= 0.000000000000090000):
        print("[ETH][CONTRACT][ERROR] This transaction may have no effect. Low balance registered.")
        return False
      self._eth_lib._w3.personal.unlockAccount( account._hash, account._password, 30 )
      return self._contract.transact({ 
        "from": account._hash
      }).__getattr__(function)(*params)
    except:
      print("[ETH][CONTRACT][ERROR] Could not execute transaction.")
    return False
