
import json
import web3
import ast
import time
import math

from web3 import Web3

from mxeth.account import Account
from mxeth.contract import Contract

class ETH:

  _w3 = None
  _private_key = None
  _public_key = None
  _id = ""

  def __init__(self, provider, wait=False):
    self._w3 = Web3(provider)
    diff = round(time.time() - self._w3.eth.getBlock("latest").timestamp)
    #print( "[ETH] " + str(diff) + "s since last block" )
    if diff > 60:
      print("[ETH][WARNING] Node not up to date.")
      if wait: self.waitForUpdate()

  def waitForUpdate(self):
    cycle = [".  ", ".. ", "..."]
    c = 0
    while True:
      diff = round(time.time() - self._w3.eth.getBlock("latest").timestamp)
      if diff <= 60:
        print("\n[ETH] Update complete.")
        return True
      print("\r[ETH] (" + str(diff) + ") Waiting " + cycle[c], end="", flush=True)
      c = c + 1
      if c >= len(cycle): c = 0
      time.sleep(5)

  def waitForTransaction(self, tx_hash, timeout=60):
    c = 1
    while True:
      transaction = self.getTransactionReceipt(tx_hash)
      if transaction != None and self.getTransactionReceipt(tx_hash)["status"] == 1:
        time.sleep(5)
        return True
      elif transaction != None and timeout > 0 and (c*5) >= timeout:
        return False
      #print("\rWaiting (" + str(c) + ")")
      time.sleep(5)
      c = c + 1

  def getAccount(self, hash, password):
    return Account(hash, password, self)

  def getContract(self, address, abi_string, key=None):
    token_abi = json.loads(abi_string)
    return Contract(address, token_abi, self._w3.eth.contract(address=address, abi=token_abi), self, key)

  def deployContract(self, account, code, contract_name):
    # TODO: Continue with contract deploy
    compiled_code = compile_source(code)
    print(compiled_code)
    interface = compiled_code["<stdin>:" + contract_name]
    print(interface)
    return -1

  def deployContractFromFile(self, account, code_path, contract_name):
    try:
      f = open(code_path, "r+")
      code = f.read()
      f.close()
      return self.deployContract(account, code, contract_name)
    except:
      return False

  def getTransaction(self, tx_hash):
    try:
      return self._w3.eth.getTransaction(tx_hash)
    except:
      return None

  def getTransactionReceipt(self, tx_hash):
    try:
      return self._w3.eth.getTransactionReceipt(tx_hash)
    except:
      return None

  def generateKeys(self):
    random_generator = Random.new().read
    key = RSA.generate(4096, random_generator)
    public_key = key.publickey()
    self._private_key = key.exportKey('PEM').decode()
    self._public_key = public_key.exportKey('PEM').decode()
    return { "private": self._private_key, "public": self._public_key }

  def saveKeys(self, id=""):
    try:
      f = open("./.certs/private_" + id + ".pem", "w+")
      f.write(self._private_key)
      f.close()
      f = open("./.certs/public_" + id + ".pem", "w+")
      f.write(self._public_key)
      f.close()
      return True
    except:
      print("[ETH][ERROR] Could not save keys.")
      return False

  def loadKeys(self, id=""):
    try:
      f = open("./.certs/private_" + id + ".pem", "r")
      self._private_key = f.read()
      f.close()
      f = open("./.certs/public_" + id + ".pem", "r")
      self._public_key = f.read()
      f.close()
      return True
    except:
      print("[ETH][ERROR] Could not load keys. Generating new ones.")
      self.generateKeys()
      self.saveKeys()
      return False
