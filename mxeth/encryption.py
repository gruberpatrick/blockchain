
import base64
import random
import string
import hashlib

from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import PKCS1_OAEP
from solc import compile_source
from Crypto.Cipher import AES

class Encryption:

  def encryptRSA(text, given_key=None):
    if given_key == None: given_key = self._public_key
    key = RSA.importKey(given_key)
    public_key = key.publickey()
    encryptor = PKCS1_OAEP.new(public_key)
    return base64.b64encode( encryptor.encrypt(text.encode()) ).decode()

  def decryptRSA(text, private_key=None):
    if private_key == None: private_key = self._private_key
    text = base64.b64decode(text.encode())
    key = RSA.importKey(private_key)
    decryptor = PKCS1_OAEP.new(key)
    try:
      decrypted = decryptor.decrypt(text)
    except:
      print("[ETH][ERROR] Could not decrypt.")
      return False
    return decrypted.decode()

  def _pad(s):
    return s + (AES.block_size - len(s) % AES.block_size) * chr(AES.block_size - len(s) % AES.block_size)

  def _unpad(s):
    return s[:-ord(s[len(s)-1:])]

  def encryptAES(text, key):
    text = Encryption._pad(text)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(text.encode())).decode()

  def decryptAES(text, key):
    text = base64.b64decode(text)
    iv = text[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return Encryption._unpad(cipher.decrypt(text[AES.block_size:])).decode('utf-8')
