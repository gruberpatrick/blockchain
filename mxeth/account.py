
class Account:

  _hash = None
  _password = None
  _eth_lib = None

  def __init__(self, hash, password, eth_lib):
    self._hash = hash
    self._password = password
    self._eth_lib = eth_lib

  def getBalance(self):
    return self._eth_lib._w3.eth.getBalance(self._hash) / 1000000000000000000
