

from web3 import HTTPProvider, IPCProvider

from mxeth.encryption import Encryption
from mxeth.account import Account
from mxeth.contract import Contract
from mxeth.eth import ETH

# ==============================================================================
# main functions
# NOTE: Current version is read only for blockchain, as infura permissions;

def testGetContract():
  global eth, c, a
  while True:
    try:
      return c.call("get", True)
    except:
      print("no good")

def testChangeContract(value):
  global eth, c, a
  return c.transact(a, "set", Encryption.encryptAES(value, c._key))

def testDeployContract():
  global eth, c, a
  eth.deployContractFromFile(a, "./contracts/basic.sol", "SimpleStorage")

# ==============================================================================

eth = ETH(IPCProvider("/home/p/.ethereum/geth.ipc"), True)
a = eth.getAccount(eth._w3.personal.listAccounts[0], "ea36df234b70181d")
c = eth.getContract("0x7a7B68767aF3FCEed3027370d0F81f13A362dc89", '[ { "constant": false, "inputs": [ { "name": "x", "type": "string" } ], "name": "set", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "get", "outputs": [ { "name": "", "type": "string", "value": "" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [ { "name": "", "type": "address", "value": "0x74379c6579262c1ac9f3ef315212490bd178d91e" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "newOwner", "type": "address" } ], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "new_value", "type": "string" } ], "name": "changed", "type": "event" } ]', b'\x02:\x02\x7f\xb3\xee\x98>)\xe8\xad\xe1\xae,\xe2q4\xfe\xdcb)\xc8\x91\xe0\x0f\x89\xb74\tE \xdb')
print("Account balance: " + str(a.getBalance()))

'''tx_hash = testChangeContract("Early one mornin' while makin' the rounds\nI took a shot of cocaine and shot my woman down\nI went right home and I went to bed\nI stuck that lovin' forty-four beneath my head.\nGot up next mornin' and I grabbed that gun\nTook a shot of cocaine and away I run\nMade a good run but I run too slow\nThey overtook me down in Juarez Mexico.\nLaid in the hot joint takin' the pill\nIn walked a sheriff from Jericho Hill\nHe said Willy Lee your name is not Jack Brown\nYou're the dirty hack that shot your woman down.\nI said yes sir my name is Willie Lee\nIf you've got a warrant just read it to me\nShot her down cause she made me slow\nI thought I was her daddy but she had five more.\nWhen I was arrested I was dressed in black\nThey put me on a train and they took me back\nHad no friends for to go my bail\nThey slapped my dried up carcass in the county jail.\nEarly next morning about a half past nine\nI spied a sheriff comin' down the line\nCoughed and coughed as he cleared his throat\nHe said come on you dirty hack into that district court\nInto the court room my trial began\nWhere I was handled by twelve honest men\nJust before the jury started out\nI saw that little judge commence to look about.\nIn about five minutes in walked a man\nHolding the verdict in his right hand\nThe verdict read in the first degree\nI hollered Lordy Lordy have mercy on me\nThe judge he smiled as he picked up his pen\nNinety nine years in the Folsom pen\nNinety nine years underneath that ground\nI can't forget the day I shot that bad bitch down\nCome on you gotta listen unto me\nLay off that whiskey and let that cocaine be")
result = eth.waitForTransaction(tx_hash, 0)
if result:
  print( testGetContract() )
else:
  print("'waitForTransaction' has timed out.")'''

#print( testDeployContract() )
