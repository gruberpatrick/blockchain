
pragma solidity ^0.4.0;
contract owned {
    address public owner;
    function owned() public {
        owner = msg.sender;
    }
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    function transferOwnership(address newOwner) onlyOwner public {
        owner = newOwner;
    }
}
contract SimpleStorage {
    string storedData;
    event changed(string data);
    function set(string x) onlyOwner public {
        storedData = x;
        changed(x);
    }
    function get() public constant returns (string) {
        return storedData;
    }
}
