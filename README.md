
# Project Blockchain

State of research can be seen here: 
https://docs.google.com/document/d/1-g6eahQjgIkQ1NE5D-iVwKO0swc6SDSRv5gGK3ez0Ks



## Initialization

Before any code can be run successfully, an ethereum node needs to be started.
This can be done through a command like (--testnet connects to Ropsten):
```geth --testnet --syncmode light --cache=512 --ipcpath /home/p/.ethereum/geth.ipc```

Infura: 
```eth = ETH(Web3(HTTPProvider("https://ropsten.infura.io/Z1Rq7nMoJbAX39aAzyKI")))```

Local .ipc connection (easiest if ethereumwallet is installed):
```eth = ETH(Web3(IPCProvider("/home/p/.ethereum/geth.ipc")))```
This method will allow access to accounts that are set up in the wallet.



## Ethereum interaction

As the deployed contracts may become more advanced, these functions may be 
subject to change.

### Initialization
```def __init__(self, w3, wait=False):```
Here, ``w3`` takes a ETH initialization of the Web3 provider. The class can then
automatically ``wait`` for the given node to be up-to-date. This is neccessary
for calls to the Ethereum network.

### Retrieving a contract
```class Contract = def getContract(self, address, abi_string, encrypted=False):```
If the ``encrypted`` flag is set, the contract specific keys will be loaded. This
makes contract handling faster.

### Deploying a contract
```class Contract = def deployContract(self, address, code):```
```class Contract = def deployContractFromFile(self, address, code_file):```
Just reads the file and calls the ``deployContract`` function.

### Initializing an account
``` class Account = def getAccount(self, hash, password):```
Initializes a new ``Account`` object and ties it to the ``ETH`` class. This allows
for easier account handling an cleaner code.



## Encryption / Decryption

This library uses the pycrypto RSA implementation to encrypt and decrypt data
that is being put on the blockchain.

### Generating new public and private keys
```{public, private} = def generateKeys(self):```
Generates both public and private key within the class and returns them. The keys
are also accessible through the ``_private_key`` and ``_public_key`` attributes.

### Saving keys locally
```def saveKeys(self, id=""):```
Here, the ``id`` parameter will identify the key for a given transaction. This
will allow for different keys for each transaction.

### Loading local keys 
```def loadKeys(self, id=""):```
If the keys could not be found, the library will automatically create a new key
pair and save them.

### Encrypt a string
```def encrypt(self, text, public_key=None):```

### Decrypt a string
```def decrypt(self, text, private_key=None):```



## Account
To be written



## Contract
To be written
